var http = require('http');
var Q = require('q')
var request = Q.denodeify(require('request'))

function StatsService() {
}

StatsService.prototype.getBroadcaster = function( broadcasterId ) {

   //var host = "http://ec2-52-24-186-22.us-west-2.compute.amazonaws.com";
   var host = "http://dev-trinity.azubu.tv";
   var path = "/api/entrypoint/broadcaster?broadcaster=" + broadcasterId;

   var uri = host + path;
   var promise = getUrl( uri );

   return promise.then( function( json ) {
      var mainAccount =  json._links[0]["main-account"];
      var promise = getUrl(mainAccount);
      return promise.then(function(json) {
         
         var achievement = json["_links"].achievement;
         var recent = json["_links"].recent;
         var record = json["_links"].record;

         var achievementPromise = getUrl( achievement );
         var recentPromise = getUrl( recent );
         var recordPromise = getUrl( record );

         return Q.all([achievementPromise, recentPromise, recordPromise])
            .spread(function( achievement, recent, record ) {
               var obj = {};
               obj.achievement = achievement;
               obj.recent = recent;
               obj.record = record;
               return obj;
            });
      });
   });
   
}

function getUrl( uri ) {
   var response = request({
      'uri': uri,
      'method': 'GET',
      'withCredentials': false // this is the important part
   });

   return response.then( function(res) {
      if (res.statusCode >= 300) 
         throw new Error('Server responded with status code ' + res.statusCode)

      return JSON.parse(res[1]);
   });
}

module.exports = StatsService;
